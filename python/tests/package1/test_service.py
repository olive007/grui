from grui import GruiService
from grui.decorator import *


class TestService(GruiService):

    @Get("hello")
    def welcome(self) -> str:
        return "Hello World!"
