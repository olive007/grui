import unittest
from grui.app import *

import package1
import package2


class TestGruiApp(unittest.TestCase):

    def test_create_empty_app(self):
        app = GruiApp()
        self.assertEqual("tests", app.title)
        self.assertEqual(1, len(app.registered_service))
        self.assertEqual(0, len(app.registered_function))
        self.assertEqual(0, len(app.registered_type))
        wsgi = app.build_wsgi(__name__)
        self.assertEqual(7, len(app.registered_function))
        self.assertEqual(5, len(app.registered_type))
        client = wsgi.test_client()
        self.assertEqual("tests", client.open('/api/grui/title', method='GRUI').get_json())
        self.assertEqual(5, len(client.open('/api/grui/type/all', method='GRUI').get_json()))
        self.assertEqual(7, len(client.open('/api/grui/function/all', method='GRUI').get_json()))
        self.assertEqual(404, client.open('/api/grui/service/all', method='GRUI').status_code)

    def test_create_app_with_title(self):
        app = GruiApp("Super title")
        self.assertEqual("Super title", app.title)
        self.assertEqual(1, len(app.registered_service))
        self.assertEqual(0, len(app.registered_function))
        self.assertEqual(0, len(app.registered_type))
        wsgi = app.build_wsgi(__name__)
        self.assertEqual(7, len(app.registered_function))
        self.assertEqual(5, len(app.registered_type))
        client = wsgi.test_client()
        self.assertEqual("Super title", client.open('/api/grui/title', method='GRUI').get_json())
        self.assertEqual(5, len(client.open('/api/grui/type/all', method='GRUI').get_json()))
        self.assertEqual(7, len(client.open('/api/grui/function/all', method='GRUI').get_json()))
        self.assertEqual(404, client.open('/api/grui/service/all', method='GRUI').status_code)

    def test_create_app_with_package1(self):
        app = GruiApp("Test with simple package", package1)
        self.assertEqual("Test with simple package", app.title)
        self.assertEqual(2, len(app.registered_service))
        self.assertEqual(0, len(app.registered_function))
        self.assertEqual(0, len(app.registered_type))
        wsgi = app.build_wsgi(__name__)
        self.assertEqual(8, len(app.registered_function))
        self.assertEqual(5, len(app.registered_type))
        client = wsgi.test_client()
        self.assertEqual("Test with simple package", client.open('/api/grui/title', method='GRUI').get_json())
        self.assertEqual(5, len(client.open('/api/grui/type/all', method='GRUI').get_json()))
        self.assertEqual(8, len(client.open('/api/grui/function/all', method='GRUI').get_json()))
        self.assertEqual(1, len(client.open('/api/grui/service/all', method='GRUI').get_json()))
        self.assertEqual("Hello World!", client.get('/api/test/hello').get_json())

    def test_create_app_with_package2(self):
        app = GruiApp("Test with package and subpackage", package2)
        self.assertEqual("Test with package and subpackage", app.title)
        self.assertEqual(4, len(app.registered_service))
        self.assertEqual(0, len(app.registered_function))
        self.assertEqual(0, len(app.registered_type))
        wsgi = app.build_wsgi(__name__)
        self.assertEqual(10, len(app.registered_function))
        self.assertEqual(5, len(app.registered_type))
        client = wsgi.test_client()
        self.assertEqual("Test with package and subpackage", client.open('/api/grui/title', method='GRUI').get_json())
        self.assertEqual(5, len(client.open('/api/grui/type/all', method='GRUI').get_json()))
        self.assertEqual(10, len(client.open('/api/grui/function/all', method='GRUI').get_json()))
        self.assertEqual(3, len(client.open('/api/grui/service/all', method='GRUI').get_json()))
        self.assertEqual("Hello world from parent", client.get('/api/parent/hello').get_json())
        self.assertEqual("Hello world from parent and child service", client.get('/api/child/hello').get_json())
        self.assertEqual("Hello world from parent and sub child service", client.get('/api/sub-child/hello').get_json())
