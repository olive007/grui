import unittest
import json

from grui.typing import *


class Model(GruiModel):
    name: str


class ModelWithDefaultValue(Model):

    def __init__(self, name="defaultName", **kwargs):
        super().__init__(**kwargs)
        self.name = name


class ModelWithOtherIdType(GruiModel):
    id: Optional[str]
    name: str


class InheritedModel(Model):
    price: float


class ModelRelated(GruiModel):
    name: str
    parent: Model


class ModelSelfRelated(GruiModel):
    name: str
    parent: "ModelSelfRelated"


class ModelSelfRelatedOptional(GruiModel):
    name: str
    parent: Optional["ModelSelfRelatedOptional"]


class ModelWithProperties(GruiModel):
    id: int
    name: str

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.__name = "Toto"

    @property
    def name(self):
        return self.__name


class TestGruiModel(unittest.TestCase):

    def test_grui_type_create(self):
        grui_type_int = GruiTypeInt(int)
        self.assertEqual(grui_type_int, GruiType.get_instance(int))
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(int)))

    # Unfortunately if someone do this the code won't detect the error
    # def test_grui_type_create_wrong1(self):
    #     grui_type_wrong = GruiTypeFloat(int)
    #     print(grui_type_wrong, GruiType.get_instance(int))
    #     self.assertEqual(grui_type_wrong, GruiType.get_instance(int))
    #     self.assertEqual(id(grui_type_wrong), id(GruiType.get_instance(int)))

    def test_grui_type_create_wrong2(self):
        GruiType.get_instance(int)
        with self.assertRaises(RuntimeError) as cm:
            GruiTypeFloat(int)
            self.assertEqual('Trying to override a type', str(cm.exception))

    def test_grui_type_int_validate(self):
        grui_type_int = GruiType.get_instance(int)

        self.assertFalse(grui_type_int.validate_value(None)[0])
        valid, value = grui_type_int.validate_value(1)
        self.assertTrue(valid)
        self.assertEqual(1, value)
        valid, value = grui_type_int.validate_value("1")
        self.assertTrue(valid)
        self.assertEqual(1, value)
        self.assertFalse(grui_type_int.validate_value("asdsad")[0])
        self.assertFalse(grui_type_int.validate_value("as10dsad")[0])
        self.assertFalse(grui_type_int.validate_value("10dsad")[0])
        self.assertFalse(grui_type_int.validate_value("as10")[0])

        valid, value = grui_type_int.validate_value("0xF")
        self.assertTrue(valid)
        self.assertEqual(15, value)
        valid, value = grui_type_int.validate_value("0x10")
        self.assertTrue(valid)
        self.assertEqual(16, value)
        self.assertFalse(grui_type_int.validate_value("0xG")[0])

        valid, value = grui_type_int.validate_value("0o10")
        self.assertTrue(valid)
        self.assertEqual(8, value)
        self.assertFalse(grui_type_int.validate_value("0o8")[0])

        valid, value = grui_type_int.validate_value("0b10")
        self.assertTrue(valid)
        self.assertEqual(2, value)
        self.assertFalse(grui_type_int.validate_value("0b2")[0])

    def test_grui_type_float_validate(self):
        grui_type_float = GruiType.get_instance(float)

        self.assertFalse(grui_type_float.validate_value(None)[0])
        valid, value = grui_type_float.validate_value(.1)
        self.assertTrue(valid)
        self.assertEqual(.1, value)
        valid, value = grui_type_float.validate_value(".1")
        self.assertTrue(valid)
        self.assertEqual(.1, value)
        valid, value = grui_type_float.validate_value("52.185")
        self.assertTrue(valid)
        self.assertEqual(52.185, value)

        self.assertFalse(grui_type_float.validate_value("0x10")[0])
        self.assertFalse(grui_type_float.validate_value("0o10")[0])
        self.assertFalse(grui_type_float.validate_value("0b10")[0])

    def test_grui_model(self):

        model = Model(id=1, name="Toto")
        model_with_default = ModelWithDefaultValue()
        model_related = ModelRelated(id=2, name="Titi", parent=model)
        model_self_related1 = ModelSelfRelated(id=3, name="Tata")
        model_self_related1.parent = model_self_related1
        model_self_related2 = ModelSelfRelated(id=4, name="Tutu", parent=model_self_related1)
        model_self_related_optional = ModelSelfRelatedOptional(id=5, name="Tete")

        self.assertEqual(1, model.to_json()["id"])
        self.assertEqual("defaultName", model_with_default.to_json()["name"])
        self.assertEqual(2, model_related.to_json()["id"])
        self.assertEqual(3, model_self_related1.to_json()["id"])
        self.assertEqual(4, model_self_related2.to_json()["id"])
        self.assertEqual(5, model_self_related_optional.to_json()["id"])

        self.assertEqual("Toto", model.to_json()["name"])

        self.assertEqual("Titi", model_related.to_json()["name"])
        self.assertEqual(1, model_related.to_json()["parent"])

        self.assertEqual("Tata", model_self_related1.to_json()["name"])
        self.assertEqual(3, model_self_related1.to_json()["parent"])

        self.assertEqual("Tutu", model_self_related2.to_json()["name"])
        self.assertEqual(3, model_self_related2.to_json()["parent"])

        self.assertEqual("Tete", model_self_related_optional.to_json()["name"])
        self.assertIsNone(model_self_related_optional.to_json()["parent"])

    def test_grui_type_int(self):

        grui_type_int = GruiType.get_instance(int)
        self.assertEqual(grui_type_int, GruiType.get_instance(int))
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(int)))
        self.assertEqual("__int__", GruiType.get_instance(int).name)
        self.assertTrue(GruiType.get_instance(int).primitive)
        self.assertFalse(GruiType.get_instance(int).model)
        self.assertFalse(GruiType.get_instance(int).container)
        self.assertFalse(GruiType.get_instance(int).nullable)
        self.assertFalse(GruiType.get_instance(int).multiple)

    def test_grui_type_int_optional(self):

        grui_type_int = GruiType.get_instance(int)
        self.assertEqual(grui_type_int, GruiType.get_instance(int))
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(int)))
        self.assertEqual("__int__", GruiType.get_instance(int).name)
        self.assertTrue(GruiType.get_instance(int).primitive)
        self.assertFalse(GruiType.get_instance(int).model)
        self.assertFalse(GruiType.get_instance(int).container)
        self.assertFalse(GruiType.get_instance(int).nullable)
        self.assertFalse(GruiType.get_instance(int).multiple)

        grui_type_int_optional = GruiType.get_instance(Optional[int])
        self.assertEqual(grui_type_int_optional, GruiType.get_instance(Optional[int]))
        self.assertEqual(id(grui_type_int_optional), id(GruiType.get_instance(Optional[int])))
        self.assertEqual("__int__", GruiType.get_instance(Optional[int]).name)
        self.assertTrue(GruiType.get_instance(Optional[int]).primitive)
        self.assertFalse(GruiType.get_instance(Optional[int]).model)
        self.assertFalse(GruiType.get_instance(Optional[int]).container)
        self.assertTrue(GruiType.get_instance(Optional[int]).nullable)
        self.assertFalse(GruiType.get_instance(Optional[int]).multiple)

    def test_grui_type_int_list(self):

        grui_type_int_list = GruiType.get_instance(List[int])
        self.assertEqual(grui_type_int_list, GruiType.get_instance(List[int]))
        self.assertEqual(id(grui_type_int_list), id(GruiType.get_instance(List[int])))
        self.assertEqual("__array__(__int__)", GruiType.get_instance(List[int]).name)
        self.assertTrue(GruiType.get_instance(List[int]).primitive)
        self.assertFalse(GruiType.get_instance(List[int]).model)
        self.assertTrue(GruiType.get_instance(List[int]).container)
        self.assertFalse(GruiType.get_instance(List[int]).nullable)
        self.assertFalse(GruiType.get_instance(List[int]).multiple)

    def test_grui_type_model(self):

        grui_type_model = GruiType.get_instance(Model)
        self.assertEqual(grui_type_model, GruiType.get_instance(Model))
        self.assertEqual(id(grui_type_model), id(GruiType.get_instance(Model)))
        self.assertEqual("model", GruiType.get_instance(Model).name)
        self.assertFalse(GruiType.get_instance(Model).primitive)
        self.assertTrue(GruiType.get_instance(Model).model)
        self.assertFalse(GruiType.get_instance(Model).container)
        self.assertFalse(GruiType.get_instance(Model).nullable)
        self.assertFalse(GruiType.get_instance(Model).multiple)

    def test_grui_type_model_optional(self):

        grui_type_model_optional = GruiType.get_instance(Optional[Model])
        self.assertEqual(grui_type_model_optional, GruiType.get_instance(Optional[Model]))
        self.assertEqual(id(grui_type_model_optional), id(GruiType.get_instance(Optional[Model])))
        self.assertEqual("model", GruiType.get_instance(Optional[Model]).name)
        self.assertFalse(GruiType.get_instance(Optional[Model]).primitive)
        self.assertTrue(GruiType.get_instance(Optional[Model]).model)
        self.assertFalse(GruiType.get_instance(Optional[Model]).container)
        self.assertTrue(GruiType.get_instance(Optional[Model]).nullable)
        self.assertFalse(GruiType.get_instance(Optional[Model]).multiple)

    def test_grui_type_get_instance(self):
        grui_type_unknown = GruiType.get_instance(None)
        self.assertFalse(grui_type_unknown.nullable)
        self.assertEqual(grui_type_unknown, GruiType.get_instance(None))
        self.assertEqual(grui_type_unknown, GruiType.get_instance(Any))
        self.assertEqual(id(grui_type_unknown), id(GruiType.get_instance(None)))
        self.assertEqual("__unknown__", GruiType.get_instance(None).name)
        self.assertFalse(GruiType.get_instance(None).primitive)
        self.assertFalse(GruiType.get_instance(None).model)
        self.assertFalse(GruiType.get_instance(None).container)
        self.assertFalse(GruiType.get_instance(None).nullable)
        self.assertFalse(GruiType.get_instance(None).multiple)

        grui_type_none = GruiType.get_instance(type(None))
        self.assertEqual(grui_type_none, GruiType.get_instance(type(None)))
        self.assertEqual(id(grui_type_none), id(GruiType.get_instance(type(None))))
        self.assertEqual("__null__", GruiType.get_instance(type(None)).name)
        self.assertTrue(GruiType.get_instance(type(None)).primitive)
        self.assertFalse(GruiType.get_instance(type(None)).model)
        self.assertFalse(GruiType.get_instance(type(None)).container)
        self.assertTrue(GruiType.get_instance(type(None)).nullable)
        self.assertFalse(GruiType.get_instance(type(None)).multiple)

        grui_type_int = GruiType.get_instance(int)
        self.assertEqual(grui_type_int, GruiType.get_instance(int))
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(int)))
        self.assertEqual("__int__", GruiType.get_instance(int).name)
        self.assertTrue(GruiType.get_instance(int).primitive)
        self.assertFalse(GruiType.get_instance(int).model)
        self.assertFalse(GruiType.get_instance(int).container)
        self.assertFalse(GruiType.get_instance(int).nullable)
        self.assertFalse(GruiType.get_instance(int).multiple)

        grui_type_float = GruiType.get_instance(float)
        self.assertEqual(grui_type_float, GruiType.get_instance(float))
        self.assertEqual(id(grui_type_float), id(GruiType.get_instance(float)))
        self.assertEqual("__float__", GruiType.get_instance(float).name)
        self.assertTrue(GruiType.get_instance(float).primitive)
        self.assertFalse(GruiType.get_instance(float).model)
        self.assertFalse(GruiType.get_instance(float).container)
        self.assertFalse(GruiType.get_instance(float).nullable)
        self.assertFalse(GruiType.get_instance(float).multiple)

        grui_type_bool = GruiType.get_instance(bool)
        self.assertEqual(grui_type_bool, GruiType.get_instance(bool))
        self.assertEqual(id(grui_type_bool), id(GruiType.get_instance(bool)))
        self.assertEqual("__bool__", GruiType.get_instance(bool).name)
        self.assertTrue(GruiType.get_instance(bool).primitive)
        self.assertFalse(GruiType.get_instance(bool).model)
        self.assertFalse(GruiType.get_instance(bool).container)
        self.assertFalse(GruiType.get_instance(bool).nullable)
        self.assertFalse(GruiType.get_instance(bool).multiple)

        grui_type_str = GruiType.get_instance(str)
        self.assertEqual(grui_type_str, GruiType.get_instance(str))
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(str)))
        self.assertEqual("__str__", GruiType.get_instance(str).name)
        self.assertTrue(GruiType.get_instance(str).primitive)
        self.assertFalse(GruiType.get_instance(str).model)
        self.assertFalse(GruiType.get_instance(str).container)
        self.assertFalse(GruiType.get_instance(str).nullable)
        self.assertFalse(GruiType.get_instance(str).multiple)

        grui_type_nullable_int = GruiType.get_instance(Optional[int])
        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(Optional[int]))
        self.assertNotEqual(grui_type_nullable_int, grui_type_int)
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(Optional[int])))
        self.assertEqual("__int__", GruiType.get_instance(Optional[int]).name)
        self.assertTrue(GruiType.get_instance(Optional[int]).primitive)
        self.assertFalse(GruiType.get_instance(Optional[int]).model)
        self.assertFalse(GruiType.get_instance(Optional[int]).container)
        self.assertTrue(GruiType.get_instance(Optional[int]).nullable)
        self.assertFalse(GruiType.get_instance(Optional[int]).multiple)

        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(Union[int, None]))
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(Union[int, None])))
        self.assertEqual("__int__", GruiType.get_instance(Union[int, None]).name)
        self.assertTrue(GruiType.get_instance(Union[int, None]).primitive)
        self.assertFalse(GruiType.get_instance(Union[int, None]).model)
        self.assertFalse(GruiType.get_instance(Union[int, None]).container)
        self.assertTrue(GruiType.get_instance(Union[int, None]).nullable)
        self.assertFalse(GruiType.get_instance(Union[int, None]).multiple)

        grui_type_nullable_str = GruiType.get_instance(Optional[str])
        self.assertEqual(grui_type_nullable_str, GruiType.get_instance(Optional[str]))
        self.assertEqual(id(grui_type_nullable_str), id(GruiType.get_instance(Optional[str])))
        self.assertEqual("__str__", GruiType.get_instance(Optional[str]).name)
        self.assertTrue(GruiType.get_instance(Optional[str]).primitive)
        self.assertFalse(GruiType.get_instance(Optional[str]).model)
        self.assertFalse(GruiType.get_instance(Optional[str]).container)
        self.assertTrue(GruiType.get_instance(Optional[str]).nullable)
        self.assertFalse(GruiType.get_instance(Optional[str]).multiple)

        grui_type_multiple = GruiType.get_instance(Union[int, float, bool])
        self.assertEqual(grui_type_multiple, GruiType.get_instance(Union[int, float, bool]))
        self.assertEqual(id(grui_type_multiple), id(GruiType.get_instance(Union[int, float, bool])))
        self.assertEqual("__multiple__(__int__, __float__, __bool__)", GruiType.get_instance(Union[int, float, bool]).name)
        self.assertTrue(GruiType.get_instance(Union[int, float, bool]).primitive)
        self.assertFalse(GruiType.get_instance(Union[int, float, bool]).model)
        self.assertFalse(GruiType.get_instance(Union[int, float, bool]).container)
        self.assertFalse(GruiType.get_instance(Union[int, float, bool]).nullable)
        self.assertTrue(GruiType.get_instance(Union[int, float, bool]).multiple)
        self.assertEqual(3, len(GruiType.get_instance(Union[int, float, bool]).choices))
        self.assertEqual(GruiType.get_instance(Union[int, float, bool]).choices[0], grui_type_int)
        self.assertEqual(id(GruiType.get_instance(Union[int, float, bool]).choices[0]), id(grui_type_int))
        self.assertEqual(GruiType.get_instance(Union[int, float, bool]).choices[1], grui_type_float)
        self.assertEqual(id(GruiType.get_instance(Union[int, float, bool]).choices[1]), id(grui_type_float))
        self.assertEqual(GruiType.get_instance(Union[int, float, bool]).choices[2], grui_type_bool)
        self.assertEqual(id(GruiType.get_instance(Union[int, float, bool]).choices[2]), id(grui_type_bool))

        grui_type_nullable_multiple = GruiType.get_instance(Union[int, float, None])
        self.assertEqual(grui_type_nullable_multiple, GruiType.get_instance(Union[int, float, None]))
        self.assertEqual(id(grui_type_nullable_multiple), id(GruiType.get_instance(Union[int, float, None])))
        self.assertEqual("__multiple__(__int__, __float__)", GruiType.get_instance(Union[int, float, None]).name)
        self.assertTrue(GruiType.get_instance(Union[int, float, None]).primitive)
        self.assertFalse(GruiType.get_instance(Union[int, float, None]).model)
        self.assertFalse(GruiType.get_instance(Union[int, float, None]).container)
        self.assertTrue(GruiType.get_instance(Union[int, float, None]).nullable)
        self.assertTrue(GruiType.get_instance(Union[int, float, None]).multiple)
        self.assertEqual(GruiType.get_instance(Union[int, float, None]).choices[0], grui_type_int)
        self.assertEqual(2, len(GruiType.get_instance(Union[int, float, None]).choices))
        self.assertEqual(GruiType.get_instance(Union[int, float, None]).choices[0], grui_type_int)
        self.assertEqual(id(GruiType.get_instance(Union[int, float, None]).choices[0]), id(grui_type_int))
        self.assertEqual(GruiType.get_instance(Union[int, float, None]).choices[1], grui_type_float)
        self.assertEqual(id(GruiType.get_instance(Union[int, float, None]).choices[1]), id(grui_type_float))

        grui_type_list_int = GruiType.get_instance(List[int])
        self.assertEqual(grui_type_list_int, GruiType.get_instance(List[int]))
        self.assertEqual(id(grui_type_list_int), id(GruiType.get_instance(List[int])))
        self.assertEqual("__array__(__int__)", GruiType.get_instance(List[int]).name)
        self.assertTrue(GruiType.get_instance(List[int]).primitive)
        self.assertFalse(GruiType.get_instance(List[int]).model)
        self.assertTrue(GruiType.get_instance(List[int]).container)
        self.assertFalse(GruiType.get_instance(List[int]).nullable)
        self.assertFalse(GruiType.get_instance(List[int]).multiple)

        grui_type_list_float = GruiType.get_instance(List[float])
        self.assertEqual(grui_type_list_float, GruiType.get_instance(List[float]))
        self.assertEqual(id(grui_type_list_float), id(GruiType.get_instance(List[float])))
        self.assertEqual("__array__(__float__)", GruiType.get_instance(List[float]).name)
        self.assertTrue(GruiType.get_instance(List[float]).primitive)
        self.assertFalse(GruiType.get_instance(List[float]).model)
        self.assertTrue(GruiType.get_instance(List[float]).container)
        self.assertFalse(GruiType.get_instance(List[float]).nullable)
        self.assertFalse(GruiType.get_instance(List[float]).multiple)

        grui_type_model = GruiType.get_instance(Model)
        self.assertEqual(grui_type_model, GruiType.get_instance(Model))
        self.assertEqual(id(grui_type_model), id(GruiType.get_instance(Model)))
        self.assertEqual("model", GruiType.get_instance(Model).name)
        self.assertFalse(GruiType.get_instance(Model).primitive)
        self.assertTrue(GruiType.get_instance(Model).model)
        self.assertFalse(GruiType.get_instance(Model).container)
        self.assertFalse(GruiType.get_instance(Model).nullable)
        self.assertFalse(GruiType.get_instance(Model).multiple)
        self.assertEqual(2, len(GruiType.get_instance(Model).children))
        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(Model).children["id"].type)
        self.assertTrue(GruiType.get_instance(Model).children["id"].read_only)
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(Model).children["id"].type))
        self.assertEqual(grui_type_str, GruiType.get_instance(Model).children["name"].type)
        self.assertFalse(GruiType.get_instance(Model).children["name"].read_only)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(Model).children["name"].type))

        grui_type_model_inherited = GruiType.get_instance(InheritedModel)
        self.assertEqual(grui_type_model_inherited, GruiType.get_instance(InheritedModel))
        self.assertEqual(id(grui_type_model_inherited), id(GruiType.get_instance(InheritedModel)))
        self.assertEqual("inherited_model", GruiType.get_instance(InheritedModel).name)
        self.assertFalse(GruiType.get_instance(InheritedModel).primitive)
        self.assertTrue(GruiType.get_instance(InheritedModel).model)
        self.assertFalse(GruiType.get_instance(InheritedModel).container)
        self.assertFalse(GruiType.get_instance(InheritedModel).nullable)
        self.assertFalse(GruiType.get_instance(InheritedModel).multiple)
        self.assertEqual(3, len(GruiType.get_instance(InheritedModel).children))
        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(InheritedModel).children["id"].type)
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(InheritedModel).children["id"].type))
        self.assertEqual(grui_type_str, GruiType.get_instance(InheritedModel).children["name"].type)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(InheritedModel).children["name"].type))
        self.assertEqual(grui_type_float, GruiType.get_instance(InheritedModel).children["price"].type)
        self.assertEqual(id(grui_type_float), id(GruiType.get_instance(InheritedModel).children["price"].type))

        grui_type_list_model = GruiType.get_instance(List[Model])
        self.assertEqual(grui_type_list_model, GruiType.get_instance(List[Model]))
        self.assertEqual(id(grui_type_list_model), id(GruiType.get_instance(List[Model])))
        self.assertEqual("__array__(model)", GruiType.get_instance(List[Model]).name)
        self.assertFalse(GruiType.get_instance(List[Model]).primitive)
        self.assertTrue(GruiType.get_instance(List[Model]).model)
        self.assertTrue(GruiType.get_instance(List[Model]).container)
        self.assertFalse(GruiType.get_instance(List[Model]).nullable)
        self.assertFalse(GruiType.get_instance(List[Model]).multiple)
        self.assertEqual(grui_type_model, GruiType.get_instance(List[Model]).child)
        self.assertEqual(id(grui_type_model), id(GruiType.get_instance(List[Model]).child))

        grui_type_list_model_inherited = GruiType.get_instance(List[Model])
        self.assertEqual(grui_type_list_model_inherited, GruiType.get_instance(List[Model]))
        self.assertEqual(id(grui_type_list_model_inherited), id(GruiType.get_instance(List[Model])))
        self.assertEqual("__array__(model)", GruiType.get_instance(List[Model]).name)
        self.assertFalse(GruiType.get_instance(List[Model]).primitive)
        self.assertTrue(GruiType.get_instance(List[Model]).model)
        self.assertTrue(GruiType.get_instance(List[Model]).container)
        self.assertFalse(GruiType.get_instance(List[Model]).nullable)
        self.assertFalse(GruiType.get_instance(List[Model]).multiple)
        self.assertEqual(grui_type_model, GruiType.get_instance(List[Model]).child)
        self.assertEqual(id(grui_type_model), id(GruiType.get_instance(List[Model]).child))
        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(ModelRelated).children["id"].type)
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(ModelRelated).children["id"].type))
        self.assertEqual(grui_type_str, GruiType.get_instance(ModelRelated).children["name"].type)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(ModelRelated).children["name"].type))

        grui_type_map_int_str = GruiType.get_instance(Dict[int, str])
        self.assertEqual(grui_type_map_int_str, GruiType.get_instance(Dict[int, str]))
        self.assertEqual(id(grui_type_map_int_str), id(GruiType.get_instance(Dict[int, str])))
        self.assertEqual("__map__(__int__, __str__)", GruiType.get_instance(Dict[int, str]).name)
        self.assertFalse(GruiType.get_instance(Dict[int, str]).primitive)
        self.assertFalse(GruiType.get_instance(Dict[int, str]).model)
        self.assertTrue(GruiType.get_instance(Dict[int, str]).container)
        self.assertFalse(GruiType.get_instance(Dict[int, str]).nullable)
        self.assertFalse(GruiType.get_instance(Dict[int, str]).multiple)
        self.assertEqual(grui_type_int, GruiType.get_instance(Dict[int, str]).key_type)
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(Dict[int, str]).key_type))
        self.assertEqual(grui_type_str, GruiType.get_instance(Dict[int, str]).value_type)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(Dict[int, str]).value_type))

        grui_type_map_str_int = GruiType.get_instance(Dict[str, int])
        self.assertEqual(grui_type_map_str_int, GruiType.get_instance(Dict[str, int]))
        self.assertEqual(id(grui_type_map_str_int), id(GruiType.get_instance(Dict[str, int])))
        self.assertNotEqual(grui_type_map_int_str, GruiType.get_instance(Dict[str, int]))
        self.assertEqual("__map__(__str__, __int__)", GruiType.get_instance(Dict[str, int]).name)
        self.assertFalse(GruiType.get_instance(Dict[str, int]).primitive)
        self.assertFalse(GruiType.get_instance(Dict[str, int]).model)
        self.assertTrue(GruiType.get_instance(Dict[str, int]).container)
        self.assertFalse(GruiType.get_instance(Dict[str, int]).nullable)
        self.assertFalse(GruiType.get_instance(Dict[str, int]).multiple)
        self.assertEqual(grui_type_str, GruiType.get_instance(Dict[str, int]).key_type)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(Dict[str, int]).key_type))
        self.assertEqual(grui_type_int, GruiType.get_instance(Dict[str, int]).value_type)
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(Dict[str, int]).value_type))

        grui_type_map_int_model = GruiType.get_instance(Dict[int, Model])
        self.assertEqual(grui_type_map_int_model, GruiType.get_instance(Dict[int, Model]))
        self.assertEqual(id(grui_type_map_int_model), id(GruiType.get_instance(Dict[int, Model])))
        self.assertEqual("__map__(__int__, model)", GruiType.get_instance(Dict[int, Model]).name)
        self.assertFalse(GruiType.get_instance(Dict[int, Model]).primitive)
        self.assertFalse(GruiType.get_instance(Dict[int, Model]).model)
        self.assertTrue(GruiType.get_instance(Dict[int, Model]).container)
        self.assertFalse(GruiType.get_instance(Dict[int, Model]).nullable)
        self.assertFalse(GruiType.get_instance(Dict[int, Model]).multiple)
        self.assertEqual(grui_type_int, GruiType.get_instance(Dict[int, Model]).key_type)
        self.assertEqual(id(grui_type_int), id(GruiType.get_instance(Dict[int, Model]).key_type))
        self.assertEqual(grui_type_model, GruiType.get_instance(Dict[int, Model]).value_type)
        self.assertEqual(id(grui_type_model), id(GruiType.get_instance(Dict[int, Model]).value_type))

        grui_type_model_with_parent = GruiType.get_instance(ModelRelated)
        self.assertEqual(grui_type_model_with_parent, GruiType.get_instance(ModelRelated))
        self.assertEqual(id(grui_type_model_with_parent), id(GruiType.get_instance(ModelRelated)))
        self.assertEqual("model_related", GruiType.get_instance(ModelRelated).name)
        self.assertFalse(GruiType.get_instance(ModelRelated).primitive)
        self.assertTrue(GruiType.get_instance(ModelRelated).model)
        self.assertFalse(GruiType.get_instance(ModelRelated).container)
        self.assertFalse(GruiType.get_instance(ModelRelated).nullable)
        self.assertFalse(GruiType.get_instance(ModelRelated).multiple)
        self.assertEqual(grui_type_nullable_int, GruiType.get_instance(ModelRelated).children["id"].type)
        self.assertEqual(id(grui_type_nullable_int), id(GruiType.get_instance(ModelRelated).children["id"].type))
        self.assertEqual(grui_type_str, GruiType.get_instance(ModelRelated).children["name"].type)
        self.assertEqual(id(grui_type_str), id(GruiType.get_instance(ModelRelated).children["name"].type))

        self.assertEqual(int, grui_type_int._original_type)
        self.assertEqual(7, len(grui_type_int.to_json()))
        self.assertEqual(int, grui_type_int._original_type)

        model = Model(id=2, name="Toto")
        json.dumps(model.to_json())
        json.dumps(GruiType.get_instance(Model).to_json())

    def test_grui_model_with_default(self):
        model_with_default = ModelWithDefaultValue()

        grui_type_model = GruiType.get_instance(ModelWithDefaultValue)

        self.assertEqual(ModelWithDefaultValue, grui_type_model._original_type)
        self.assertEqual("defaultName", grui_type_model.children["name"].default_value)
        self.assertEqual(2, len(grui_type_model.children))

    def test_grui_model_id_int(self):

        model = Model()
        self.assertIsNone(model.id)
        model.id = 8
        self.assertEqual(8, model.id)
        with self.assertRaises(UpdateIdError) as cm:
            model.id = 43
            self.assertEqual("Trying to update the id", str(cm.exception))

        model = Model(id=2)
        self.assertEqual(2, model.id)
        with self.assertRaises(UpdateIdError) as cm:
            model.id = 43
            self.assertEqual("Trying to update the id", str(cm.exception))

        model = Model(id=2)
        self.assertEqual(2, model.id)
        with self.assertRaises(IncorrectDataWhileEncodingError) as cm:
            model.to_json()
            self.assertEqual("Trying to encode with incorrect data", str(cm.exception))

        model.name = "testing"
        self.assertTrue(model.validate())
        self.assertEqual("testing", model.name)
        json = model.to_json()
        self.assertEqual("testing", model.name)
        self.assertEqual(json, model.to_json())
        self.assertEqual(id(json), id(model.to_json()))
        self.assertTrue(model.validate())

        self.assertEqual(2, model.to_json()["id"])
        self.assertEqual("testing", model.to_json()["name"])

        model = Model(id="89", name="converting")
        self.assertEqual("89", model.id)
        self.assertEqual("converting", model.name)
        self.assertEqual(89, model.to_json()["id"])
        self.assertEqual("converting", model.to_json()["name"])
        self.assertEqual(89, model.id)

    def test_grui_model_id_str(self):

        model = ModelWithOtherIdType()
        self.assertIsNone(model.id)
        model.id = "TSD:8"
        self.assertEqual("TSD:8", model.id)
        with self.assertRaises(UpdateIdError) as cm:
            model.id = "DLS:242"
            self.assertEqual("Trying to update the id", str(cm.exception))

        model = ModelWithOtherIdType(id="OAK:79")
        self.assertEqual("OAK:79", model.id)
        with self.assertRaises(UpdateIdError) as cm:
            model.id = "DLS:242"
            self.assertEqual("Trying to update the id", str(cm.exception))

        with self.assertRaises(IncorrectDataWhileEncodingError) as cm:
            model.to_json()
            self.assertEqual("Trying to encode with incorrect data", str(cm.exception))

        model.name = "testing"
        self.assertTrue(model.validate())
        json = model.to_json()
        self.assertEqual("testing", model.name)
        self.assertEqual(json, model.to_json())
        self.assertEqual(id(json), id(model.to_json()))
        self.assertTrue(model.validate())

        self.assertEqual("OAK:79", model.to_json()["id"])
        self.assertEqual("testing", model.to_json()["name"])

    def test_grui_model_update_json(self):

        model = Model(id=2, name="Toto")
        self.assertEqual(2, model.id)
        self.assertEqual("Toto", model.name)
        self.assertTrue(model.validate())

        json = model.to_json()
        self.assertEqual(json, model.to_json())
        self.assertEqual(id(json), id(model.to_json()))
        self.assertEqual(2, model.to_json()["id"])
        self.assertEqual("Toto", model.to_json()["name"])

        model.name = "Tata"
        json_updated = model.to_json()
        self.assertEqual(json_updated, model.to_json())
        self.assertEqual(id(json_updated), id(model.to_json()))
        self.assertNotEqual(json, model.to_json())
        self.assertNotEqual(id(json), id(model.to_json()))
        self.assertEqual(2, model.to_json()["id"])
        self.assertEqual("Tata", model.to_json()["name"])

    def test_grui_model_json_dumps(self):

        model = Model(id=2, name="Toto")
        self.assertEqual('{"id": 2, "name": "Toto"}', json.dumps(model.to_json()))

        model = Model(name="Toto")
        self.assertEqual('{"id": null, "name": "Toto"}', json.dumps(model.to_json()))

    def test_grui_model_with_properties(self):
        model = ModelWithProperties(id=2)

        self.assertEqual('{"id": 2, "name": "Toto"}', json.dumps(model.to_json()))

    def test_grui_model_with_other_id_type(self):
        model = ModelWithOtherIdType(name="Toto")

        self.assertEqual('{"id": null, "name": "Toto"}', json.dumps(model.to_json()))
        model = ModelWithOtherIdType(id="2", name="Toto")

        self.assertEqual('{"id": "2", "name": "Toto"}', json.dumps(model.to_json()))
