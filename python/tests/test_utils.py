import unittest

from grui.utils import *
from grui import GruiModel, GruiType


class TestJsonEncoder(unittest.TestCase):

    def test_json_encoder(self):

        class Model(GruiModel):
            name: str

        grui_type_int = GruiType.get_instance(int)
        grui_type_model = GruiType.get_instance(Model)

        res = json.dumps(grui_type_int, cls=GruiJsonEncoder)
        self.assertIn('"id"', res)
        self.assertIn('"primitive"', res)
        self.assertIn('"name"', res)
        self.assertIn('"model"', res)
        self.assertIn('"container"', res)
        self.assertIn('"multiple"', res)
        self.assertIn('"nullable"', res)
        res = json.dumps(grui_type_model, cls=GruiJsonEncoder)
        self.assertIn('"id"', res)
        self.assertIn('"primitive"', res)
        self.assertIn('"name"', res)
        self.assertIn('"model"', res)
        self.assertIn('"container"', res)
        self.assertIn('"multiple"', res)
        self.assertIn('"nullable"', res)
        self.assertIn('"children"', res)
