import unittest

from grui import GruiService, GruiModel


class ModelTest(GruiModel):
    description: str


class TwoWordsService(GruiService):
    pass


class RenamedService(GruiService):
    _url_prefix = "alias"
    pass


class DynamicRenamedService(GruiService):
    pass


class TestService(unittest.TestCase):

    def test_url_prefix(self):

        self.assertEqual("two-words", TwoWordsService.url_prefix)
        self.assertEqual("alias", RenamedService.url_prefix)
        self.assertEqual("dynamic-renamed", DynamicRenamedService.url_prefix)
        DynamicRenamedService._url_prefix = "updated-at-runtime"
        self.assertEqual("updated-at-runtime", DynamicRenamedService.url_prefix)

        def test_raising():
            RenamedService.url_prefix = "raising-error"

        self.assertRaises(AttributeError, test_raising)
