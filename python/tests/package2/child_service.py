from grui import GruiService
from grui.decorator import *

from .parent import Parent


class ChildService(GruiService):

    def __init__(self, parent_service: Parent):
        self.dependent = parent_service

    @Get("hello")
    def welcome(self) -> str:
        return self.dependent.welcome() + " and child service"

