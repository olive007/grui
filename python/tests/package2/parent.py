from grui import GruiService
from grui.decorator import *


class Parent(GruiService):

    @Get("hello")
    def welcome(self) -> str:
        return "Hello world from parent"
