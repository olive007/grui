from types import MethodType
import unittest

from grui import *


class Foo(GruiService):
    """This is a simple class to run unit test"""
    def __init__(self, people: str = "World"):
        self._people = people
        self.bar("message into init")

    @Get
    def bar(self, message):
        """This method return a welcome message"""
        return "Hello %s: %s" % (self._people, message)

    @Get
    def baz(self, message: str) -> str:
        """
        This method return a welcome message

        The function return a welcome message in french
        """
        return "Bonjour %s: %s" % (self._people, message)

    @Get
    def bax(self, message: str) -> str:
        """
        This method return a welcome message

        The function return a welcome message in french
        :param message: The message part of the user
        :return: The message in french
        """
        return "Bonjour %s: %s" % (self._people, message)


class FooPrefixed(GruiService):
    _url_prefix = ""

    @Get
    def baz(self, message):
        return "Hello" + str(message)

    @Get("titi///toto")
    def bar(self, message):
        return self.baz(message)


@Get
def my_function(message):
    """This function return a welcome message"""
    return "::%s::" % message


@Get("custom/path")
def my_function_custom(message):
    """This function return a welcome message"""
    return "::%s::" % message


def replacer_function_no_decorated(self, message):
    """This is a replaced method returning a welcome message"""
    return "Hi %s: %s" % (self._people, message)


@Get
def replacer_function_decorated(self, message):
    """This is a replaced decorated method returning a welcome message"""
    return "Hola %s: %s" % (self._people, message)


class TestDecorator(unittest.TestCase):

    def test_function_decorated(self):
        self.assertEqual("::Hello::", my_function("Hello"))
        self.assertEqual("test_decorator.my_function", my_function.__name__)
        self.assertEqual("This function return a welcome message", my_function.__doc__)

    def test_method_decorated(self):
        self.assertEqual("This is a simple class to run unit test", Foo.__doc__)
        self.assertEqual("test_decorator.Foo.bar", Foo.bar.__name__)
        self.assertEqual("test_decorator.Foo.bar", Foo().bar.__name__)
        self.assertEqual("This method return a welcome message", Foo().bar.__doc__)
        foo = Foo()
        self.assertEqual("Hello World: msg", foo.bar("msg"))
        self.assertEqual("test_decorator.Foo.bar", foo.bar.__name__)
        self.assertEqual("This method return a welcome message", foo.bar.__doc__)
        foo = Foo("Everyone")
        self.assertEqual("Hello Everyone: msg", foo.bar("msg"))
        self.assertEqual("test_decorator.Foo.bar", foo.bar.__name__)
        self.assertEqual("This method return a welcome message", foo.bar.__doc__)

    def test_method_and_function_decorated(self):
        foo = Foo()
        self.assertEqual("Hello World: msg", foo.bar("msg"))
        self.assertEqual("::Hello::", my_function("Hello"))
        foo = Foo("Everyone")
        self.assertEqual("Hello Everyone: msg", foo.bar("msg"))

    def test_method_replaced_decorated(self):
        self.assertEqual("::Hello::", my_function("Hello"))
        foo = Foo()
        self.assertEqual("Hello World: msg", foo.bar("msg"))

        foo.bar = MethodType(replacer_function_decorated, foo)
        self.assertEqual("Hola World: msg", foo.bar("msg"))
        self.assertEqual("test_decorator.Foo.bar", Foo.bar.__name__)
        self.assertEqual("This is a replaced decorated method returning a welcome message", foo.bar.__doc__)

        foo.bar = MethodType(replacer_function_no_decorated, foo)
        self.assertEqual("Hi World: msg", foo.bar("msg"))
        self.assertEqual("test_decorator.Foo.bar", Foo.bar.__name__)
        self.assertEqual("This is a replaced method returning a welcome message", foo.bar.__doc__)

        foo = Foo("Everyone")
        self.assertEqual("Hello Everyone: msg", foo.bar("msg"))
        self.assertEqual("test_decorator.Foo.bar", Foo.bar.__name__)
        self.assertEqual("This method return a welcome message", foo.bar.__doc__)

    def test_method_properties(self):
        foo = Foo()
        self.assertEqual(foo, foo.bar._GruiFunction__instance)
        self.assertEqual(Foo, foo.bar._GruiFunction__owner)
        self.assertEqual(foo.__class__, foo.bar._GruiFunction__owner)
        self.assertEqual("/api/foo", foo.bar.path)
        self.assertEqual("GET", foo.bar.method)
        self.assertEqual(1, len(foo.bar.args))
        self.assertEqual(GruiType.get_instance(None), foo.bar.args["message"].type)
        self.assertEqual("", foo.bar.args["message"].description)
        self.assertFalse(foo.bar.args["message"].optional)
        self.assertEqual("This method return a welcome message", foo.bar.presentation)
        self.assertEqual("", foo.bar.description)
        self.assertIsNone(foo.bar.result)
        self.assertEqual(200, foo.bar.default_return_code)
        self.assertEqual(0, len(foo.bar.previous_actions))
        self.assertEqual(0, len(foo.bar.next_actions))
        self.assertFalse(foo.bar.multiple_action)
        self.assertTrue(foo.bar._GruiFunction__properties_updated)

        self.assertEqual(GruiType.get_instance(str), foo.baz.args["message"].type)
        self.assertEqual("", foo.baz.args["message"].description)
        self.assertFalse(foo.baz.args["message"].optional)
        self.assertEqual("The function return a welcome message in french", foo.baz.description)
        self.assertEqual(GruiType.get_instance(str), foo.baz.result.type)
        self.assertEqual("", foo.baz.result.description)

        self.assertEqual(GruiType.get_instance(str), foo.bax.args["message"].type)
        self.assertEqual("The message part of the user", foo.bax.args["message"].description)
        self.assertFalse(foo.bax.args["message"].optional)
        self.assertEqual("The function return a welcome message in french", foo.bax.description)
        self.assertEqual(GruiType.get_instance(str), foo.bax.result.type)
        self.assertEqual("The message in french", foo.bax.result.description)

    def test_function_properties(self):
        self.assertEqual("/api/test_decorator-my_function", my_function.path)
        self.assertEqual("GET", my_function.method)
        self.assertEqual(0, len(my_function.previous_actions))
        self.assertEqual(0, len(my_function.next_actions))
        self.assertFalse(my_function.multiple_action)

    def test_function_custom_properties(self):
        self.assertEqual("/api/custom/path", my_function_custom.path)
        self.assertEqual("GET", my_function_custom.method)
        self.assertEqual(0, len(my_function_custom.previous_actions))
        self.assertEqual(0, len(my_function_custom.next_actions))
        self.assertFalse(my_function_custom.multiple_action)

    def test_function_json(self):
        foo = Foo()
        json = foo.bar.to_json()

        self.assertIn("id", json)
        self.assertEqual("/api/foo", json['path'])
        self.assertEqual("GET", json['method'])
        self.assertEqual(GruiType.get_instance(None).id, json['args']['message']['type'])
        self.assertEqual("", json['args']['message']['description'])
        self.assertEqual({}, json['args']['message']['condition'])
        self.assertFalse(json['args']['message']['optional'])
        self.assertEqual("This method return a welcome message", json['presentation'])
        self.assertEqual("", json['description'])
        self.assertIsNone(json['result'])
        self.assertFalse(json['depreciated'])

    def test_empty_url_prefix(self):
        foo = FooPrefixed()

        self.assertEqual("GET", foo.baz.method)
        self.assertEqual("/api/", foo.baz.path)

        self.assertEqual("GET", foo.bar.method)
        self.assertEqual("/api/titi/toto", foo.bar.path)
