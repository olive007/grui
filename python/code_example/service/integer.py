import csv

from grui.decorator import *
from grui import GruiService
from grui.typing import *


class IntegerService(GruiService):
    """Integer Service to present the functionality of GRUI

    Allow user to stack integer and remove them. The adding order is kept.
    """

    FILE_LOCATION = "integer.csv"

    def __init__(self):
        self._integers = []
        self.load_data()
        print(self.get_all())
        
    @Get("all")
    @NotFoundIfEmpty
    def get_all(self) -> List[int]:
        return self._integers

    @Get("<index>")
    @NotFoundIfNone
    @MultipleCallAtOnce
    def get_at_position(self, index: int) -> int:
        return self._integers[index] if index < len(self._integers) else None

    @Post
    @MultipleCallAtOnce
    def add(self, new_integer: int) -> Optional[int]:
        """Add a new number into the queue.

        Adding the new number if it is not already present. The number is added to the end.

        :param new_integer: New number added to the queue. (Max: 15000)
        :return: the new integer added or None
        """
        if new_integer not in self._integers:
            self._integers.append(new_integer)
            return new_integer
        return None

    @Put("<index_to_update>")
    @MultipleCallAtOnce
    def update_at_position(self, index_to_update: int, new_integer: int):
        if new_integer not in self._integers:
            self._integers[index_to_update] = new_integer

    @Delete("<index_to_remove>")
    @MultipleCallAtOnce
    def delete_at_position(self, index_to_remove: int):
        del self._integers[index_to_remove]

    def load_data(self):
        with open(IntegerService.FILE_LOCATION, 'r') as csv_reader:
            reader = csv.DictReader(csv_reader)
            number_loaded = 0
            for row in reader:
                self.add(int(row['integer']))
                number_loaded += 1
            if number_loaded > 0:
                print("Number loaded:", number_loaded)

    def save_data(self):
        print("Saving integer...")
        with open(IntegerService.FILE_LOCATION, 'w') as csv_writer:
            writer = csv.DictWriter(csv_writer, fieldnames=['integer'])
            writer.writeheader()
            for number in self._integers:
                writer.writerow({'integer': number})

    def __delete__(self, instance):
        self.save_data()
