from typing import *
from grui import *


class Todo(GruiModel):
    """
    Task to realize later.

    :attribute  title  : The title of the task.
    :attribute  note   : Optional explanation to realise the task.
    :attribute  price  : Optional price of the task.
    :attribute  finish : Is the task realized or not.
    """

    title: str
    note: Optional[Text]
    price: Optional[float]
    finish: bool

    def __init__(self, title="New TODO", **kwargs):
        self.note = None
        self.price = None
        self.finish = False
        kwargs["title"] = title
        super().__init__(**kwargs)


class TodoService(GruiService):
    """
    Simple Todo task register.

    This simple web service allow you to save task to do commonly call Todo.
    """

    def __init__(self):
        self.__counter = 0
        self.__todo_by_id = {}
        self.add_new(Todo("Todo example in progress"))
        self.add_new(Todo("TODO example finish", finish=True))

    @Get("all")
    @NotFoundIfEmpty
    def get_all(self) -> List[Todo]:
        """
        Retrieve all todo registered.

        :return: All the todo registered.
        """
        return list(self.__todo_by_id.values())

    @Get("<id_>")
    @NotFoundIfNone
    @MultipleCallAtOnce
    def get_by_id(self, id_: int) -> Optional[Todo]:
        """
        Retrieve todo by its id.

        :param  id_: Identifier of the task to retrieve.

        :return: The todo asked if it's registered otherwise None.
        """
        return self.__todo_by_id.get(id_, None)

    @Get("finished")
    def get_nbr_todo_finished(self) -> int:
        """
        :return: The number of todo already finished.
        """
        todo_finished = 0
        for todo in self.__todo_by_id.values():
            if todo.finish:
                todo_finished += 1
        return todo_finished

    @Post
    @MultipleCallAtOnce
    def add_new(self, new_todo: Todo) -> Optional[Todo]:
        """
        Register a new todo.

        :param  new_todo : new instance of task to register.

        :return: The new task registered if it's valid otherwise None.
        """
        if not new_todo.validate():
            return None
        self.__counter += 1
        keys = self.__todo_by_id.keys()
        if self.__counter in keys:
            self.__counter = len(keys) + 1
        while self.__counter in keys:
            self.__counter += 1
        new_todo.id = self.__counter
        self.__todo_by_id[new_todo.id] = new_todo
        return new_todo

    @Put("<id_>")
    @NotFoundIfNone
    @MultipleCallAtOnce
    def update(self, id_: int, updated_todo: Todo) -> Optional[Todo]:
        """
        Update a registered todo.

        :param id_          : id of the task to update.
        :param updated_todo : task with the updated value.

        :return: The registered task updated if it's valid otherwise None.
        """
        old_todo = self.__todo_by_id.get(id_)
        if old_todo is None:
            return None
        tmp = old_todo.update(updated_todo)
        if not tmp.validate():
            raise IncorrectDataWhileEncodingError(wrong_model=updated_todo)
        self.__todo_by_id[tmp.id] = tmp
        return tmp
