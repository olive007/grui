from grui import GruiApp
import service

app = GruiApp(service_package=service)
app.config["DEBUG"] = True

if __name__ == '__main__':
    app.run()
