from grui import GruiApp

from service.integer import IntegerService

app = GruiApp()
app.register_service(IntegerService)
app.config["DEBUG"] = True


if __name__ == '__main__':
    app.run()
