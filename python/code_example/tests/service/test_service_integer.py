import unittest
from code_example.service import IntegerService


class TestApp(unittest.TestCase):

    def setUp(self):
        self._service = IntegerService()

    def test_init(self):
        self.assertEqual(len(self._service._integers), 0)
