import { deepFreeze } from "./util"


test('deepFreeze', () => {
    let value = {
        test: "Toto"
    }
    let valueFreezed = deepFreeze(value);
    expect(Object.isFrozen(valueFreezed)).toBe(true);
});
