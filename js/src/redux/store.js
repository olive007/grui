import { createStore, applyMiddleware, compose } from "redux";
import createSagaMiddleware from "redux-saga";

import { getInitialState, getActions, getReducers, getSagaRunners } from "./action";
import { register as apiRegister } from "./api";
import { register as gruiRegister } from "../module/grui/redux";
import { register as dataRegister } from "../module/data/redux";

apiRegister()
gruiRegister()
dataRegister()


function reducer(state = getInitialState(), action) {

    let newState = Object.assign({}, state);
    const actionsDefinition = getActions();
    const reducerFunctions = getReducers();

    // console.log("store before", action, state);

    if (actionsDefinition[action.type] !== undefined && actionsDefinition[action.type]) {
        newState = reducerFunctions[action.type](newState, action.payload);
    }

    // console.log("store after ", action, newState);

    return newState;
}

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
const sagaMiddleware = createSagaMiddleware();
const store = createStore(
    reducer,
    storeEnhancers(
        applyMiddleware(sagaMiddleware)
    )
);

const runners = getSagaRunners();

for (let i = runners.length; --i >= 0;) {
    sagaMiddleware.run(runners[i]);
}

export default store;
