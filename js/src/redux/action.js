import { mergeObjects } from '../util';

// TODO reformat
// This file allow developers to generate the 'action' function for redux.
// To generate an 'action' function just call the function 'generateActionFunc'
// the result will be its 'action' function. This guaranty their uniqueness.
// They can be dispatch by redux like any other hand-written 'action' functions.


////////////////////////////////////////////////////////////////////////////////
// Getter
////////////////////////////////////////////////////////////////////////////////

/**
 * Get initial state
 */
export function getInitialState() {
    initializeGenerator();
    return initializeGenerator.initialState;
}

/**
 * Get all actions registered.
 */
export function getActions() {
    initializeGenerator();
    return initializeGenerator.actions;
}

/**
 * Get the action type for a specific module.
 */
export function getModuleActionTypes(moduleName) {
    initializeGenerator();
    return initializeGenerator.actionsByModule[moduleName]
}

/**
 * Get the reducer functions
 */
export function getReducers() {
    initializeGenerator();
    return initializeGenerator.reducers;
}

/**
 * Get the reducer functions
 */
export function getSagaRunners() {
    initializeGenerator();
    return initializeGenerator.sagaRunners;
}

////////////////////////////////////////////////////////////////////////////////
// Method
////////////////////////////////////////////////////////////////////////////////

/**
 * Register a custom initial state to the main one.
 */
export function registerInitialState(customInitialState) {
    initializeGenerator();
    initializeGenerator.initialState = mergeObjects(initializeGenerator.initialState, customInitialState);
}

/**
 * Register a reducer into the store and dynamically create the action type and function for the reducer
 *
 * @param {string}        moduleName  Name of the module
 * @param {string}        id          Id of the redux action
 * @param {Function|null} reducer     Redux reducer
 */
export function registerAction(moduleName, id, reducer = null) {
    initializeGenerator();
    // Define the action ID
    // Like we are sure that no action will be duplicated
    const actionId = (moduleName + "/" + id).toUpperCase();

    // Check if we didn't override a existing action ...
    if (initializeGenerator.actions[actionId] !== undefined) {
        // ... then send a error to the developer.
        throw new Error("You are trying to overwrite an redux action: "+ id);
    } else {
        if (initializeGenerator.actionsByModule[moduleName] === undefined) {
            initializeGenerator.actionsByModule[moduleName] = {};
        }
        initializeGenerator.actionsByModule[moduleName][id] = actionId;
        // ... otherwise we register the new action.
        // By creating a object from the reducer class we will know
        initializeGenerator.actions[actionId] = reducer !== null;
        if (reducer !== null) {
            initializeGenerator.reducers[actionId] = reducer;
        }
    }

    // Now we dynamically create the 'action' function.
    // This function will have to be dispatch by redux.
    // Redux will pass the argument from this generated function to the 'reducer' function
    const functionBody = 'return {type:"' + actionId + '", payload};';
    // eslint-disable-next-line no-new-func
    return Function("payload", functionBody);
}

/**
 * Register a saga watcher.
 */
export function registerSagaWatcher(watcher) {
    initializeGenerator();
    initializeGenerator.sagaRunners.push(watcher);
}

/**
 * Initialize the generator.
 */
function initializeGenerator() {

    if (initializeGenerator.initialState === undefined) {
        initializeGenerator.initialState = {};
        initializeGenerator.actions = {};
        initializeGenerator.reducers = {};
        initializeGenerator.actionsByModule = {};
        initializeGenerator.sagaRunners = [];
    }
}