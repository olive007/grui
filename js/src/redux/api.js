import { call, put, select } from "@redux-saga/core/effects";
import { registerInitialState, registerAction, getModuleActionTypes } from "./action";
import { deepFreeze } from "../util";

////////////////////////////////////////////////////////////////////////////////
// Shared variables
////////////////////////////////////////////////////////////////////////////////

const url = "http://127.0.0.1:5000";
let apiActions;

////////////////////////////////////////////////////////////////////////////////
// Redux module
////////////////////////////////////////////////////////////////////////////////

export function register() {

    ////////////////////////////////////////////////////////////////////////////
    // Initial State
    ////////////////////////////////////////////////////////////////////////////

    let ERROR_ID = 0
    registerInitialState({
        api: {
            errors: [],
        }
    });

    ////////////////////////////////////////////////////////////////////////////
    // Actions
    ////////////////////////////////////////////////////////////////////////////

    registerAction("api", "ERROR", (state, error) => {
        let tmp = Object.assign([], state.api.errors);
        error.id = ERROR_ID++;
        tmp.push(deepFreeze(Object.assign({}, {...error})));
        state.api.errors = Object.freeze(tmp);
        return state;
    });

    apiActions = getModuleActionTypes("api");
}

export { apiActions as default };

////////////////////////////////////////////////////////////////////////////////
// End of redux module
////////////////////////////////////////////////////////////////////////////////
// Exception
////////////////////////////////////////////////////////////////////////////////

class ApiError extends Error {

    constructor(method, msg) {
        super(msg);
        this.method = method
    }
}

////////////////////////////////////////////////////////////////////////////////
// Api call
////////////////////////////////////////////////////////////////////////////////

export function processResponse(method, path, response) {
    //console.log("test", method, path, response)
    if (response.ok) {
        return response.json();
    } else {
        // TODO
        throw new ApiError(method, "Error while getting data:" + path);
    }
}

export function apiCallGet(path) {
    console.log("GET", path);
    return fetch(url + path).then((response) => {
        return processResponse("GET", path, response);
    });
}

export function apiCallPost(path, body) {
    console.log("POST", path);
    return fetch(url + path, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => {
        return processResponse("POST", path, response);
    });
}

export function apiCallPut(path, body) {
    console.log("PUT", path);
    return fetch(url + path, {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json'
        },
        body: JSON.stringify(body)
    }).then((response) => {
        return processResponse("PUT", path, response);
    });
}

export function apiCallDelete(path) {
    console.log("DELETE", path);
    return fetch(url + path, {
        method: "DELETE"
    }).then((response) => {
        return processResponse("DELETE", path, response);
    });
}

export function apiCallGrui(path) {
    console.log("GRUI", path);
    return fetch(url + path, {
        method: "GRUI"
    }).then((response) => {
        return processResponse("GRUI", path, response);
    });
}

const METHOD_CALL = {
    "GET": apiCallGet,
    "POST": apiCallPost,
    "PUT": apiCallPut,
    "DELETE": apiCallDelete,
    "GRUI": apiCallGrui
}

export function getApiCallGyMethod(method) {
    return METHOD_CALL[method]
}

export function generateApiCall(loadedCheck, method, returnReduxType, additionalParams = {}, body = null) {
    return function* callFromApi({payload}) {
        const url = typeof(loadedCheck) === "string" ? loadedCheck : yield select(loadedCheck, payload);
        if (url !== null) {
            try {
                const response = yield call(METHOD_CALL[method], url, body);
                yield put({type: returnReduxType, payload: {...additionalParams, response, callWith: payload}});
            } catch (e) {
                console.log("eeee", e);
                yield put({type: apiActions.ERROR, payload: {url, method, payload, body, exception: e}});
            }
        }
    }
}

export function generateApiGetCall(loadedCheck, returnReduxType, additionalParams = {}) {
    return generateApiCall(loadedCheck, "GET", returnReduxType, additionalParams);
}

export function generateApiPostCall(loadedCheck, returnReduxType, additionalParams = {}, body = null) {
    return generateApiCall(loadedCheck, "POST", returnReduxType, additionalParams, body);
}

export function generateApiPutCall(loadedCheck, returnReduxType, additionalParams = {}, body = null) {
    return generateApiCall(loadedCheck, "PUT", returnReduxType, additionalParams, body);
}

export function generateApiDeleteCall(loadedCheck, returnReduxType, additionalParams = {}) {
    return generateApiCall(loadedCheck, "DELETE", returnReduxType, additionalParams);
}

export function generateApiGruiCall(loadedCheck, returnReduxType, additionalParams = {}) {
    return generateApiCall(loadedCheck, "GRUI", returnReduxType, additionalParams);
}
