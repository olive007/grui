/**
 * @param array
 * @param key
 * @param freeze
 * @returns A Map containing every entry of the array organized by the key specified
 */
export function organizeArrayToMap(array, freeze = false, key = "id") {
    let result = {}

    for (let i = array.length; --i >= 0;) {
        result[array[i][key]] = freeze ? deepFreeze(array[i]) : array[i];
    }
    return freeze ? Object.freeze(result) : result
}

export function populateDict(origin, freeze, key, ...sources) {
    let newValues = {}
    if (origin === undefined || origin === null) {
        origin = {};
    }

    for (let i = sources.length; --i >= 0;) {
        for (let j = sources[i].length; --j >= 0;) {
            newValues[sources[i][j][key]] = freeze ? deepFreeze(sources[i][j]) : sources[i][j];
        }
    }
    let result = Object.assign({}, origin, newValues);
    return freeze ? Object.freeze(result) : result
}

export function deepFreeze(obj) {
    // Retrieve the property names defined on object
    let propNames = Object.getOwnPropertyNames(obj);
    // Reassign the
    //console.log("qdqsd", typeof(obj), obj);
    if (Array.isArray(obj)) {
        return Object.freeze(obj);
    }
    let result = typeof(obj) === "object" ? Object.assign({}, obj) : obj;

    // Freeze properties before freezing self
    for (const name of propNames) {
        const value = result[name];
        result[name] = value && typeof value === "object" ? deepFreeze(value) : value;
    }

    return Object.freeze(result);
}

export const EMPTY_ARRAY = Object.freeze([]);

export function getWidth() {
    return  window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
}

export function noop() {

}

/**
 * Merge 2 object together without allowing duplicates.
 */
export function mergeObjects() {
    let res = {};

    for (let i = arguments.length; --i >= 0;) {
        let object = arguments[i];
        if (object === undefined || object === null) {
            continue;
        }
        let keys = Object.keys(object);
        let duplicateKeys = Object.keys(res).filter(x => keys.includes(x));
        if (duplicateKeys.length > 0) {
            throw new Error("Duplicate key while merging objects: " + duplicateKeys);
        }
        res = {...res, ...object};
    }
    return res;
}