// Auto adapt the color of the css with the color theme value.
// 0 is light, 1 is dark

import store from "../../redux/store";

function setColorTheme(index) {
    for (let name in colors) {
        document.documentElement.style.setProperty("--" + name + "-color", colors[name][index]);
    }
}

export function updateColorTheme(colorTheme = store.getState().grui.colorTheme) {

    switch (colorTheme) {
        case "DARK":
            setColorTheme(1);
            break
        case "LIGHT":
            setColorTheme(0);
            break
        default:
        case "AUTO":
            // Check to see if Media-Queries are supported
            if (window.matchMedia) {
                // Check if the dark-mode Media-Query matches
                if(window.matchMedia('(prefers-color-scheme: dark)').matches){
                    setColorTheme(1);
                } else {
                    setColorTheme(0);
                }
            } else {
                setColorTheme(0);
            }
    }
}

const colors = {
    black: ["#111111", "#101010"],
    grey: ["#999999", "#ababab"],
    "grey-dark": ["#727272", "#737373"],
    white: ["#d7d7d7", "#e3e3e3"],
    red: ["#cd0b0b", "#8f0808"],
    orange: ["#d26528", "#c6640f"],
    green: ["#35cd0b", "#088f0a"],
    blue: ["#3c81de", "#08258f"],

    "text-default": ["#111111", "#EEE"],
    "text-selected": ["#0e213c", "#8ca6e2"],

    "background-default": ["#EEE", "#222"],
    "background-darker": ["#c6c6c6", "#151515"],
    "background-default-selected": ["#8ca6e2", "#0e213c"],
    "background-darker-selected": ["#6376a0", "#0a182b"],
};