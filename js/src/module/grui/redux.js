import { takeEvery, select, put, fork, join, call } from "@redux-saga/core/effects";

import { registerInitialState, registerAction, registerSagaWatcher, getModuleActionTypes } from "../../redux/action";
import { updateColorTheme } from "./color";
import { generateApiCall, getApiCallGyMethod } from "../../redux/api";
import { deepFreeze, organizeArrayToMap, populateDict } from "../../util";
import apiActions from "../../redux/api";

const HTTP_METHOD_ORDER = {
    GET: 1,
    POST: 2,
    PUT: 3,
    DELETE: 4,
};

let actions;
let loadInitialData;
let changeColorTheme;
let executeRemoteFunction;

export function register() {

    ////////////////////////////////////////////////////////////////////////////
    // Initial State
    ////////////////////////////////////////////////////////////////////////////

    registerInitialState({
        grui: {
            title: null,
            paths: [["home"]],
            currentPath: ["toto", "tata"],
            initialized: false,
            colorTheme: "AUTO",
            service: {
                byId: {},
                selected: null,
                defaultActions: {}
            },
            function: {
                byId: {},
                order: [],
                results: {}
            },
            type: {},
            users: {
                authenticated: null,
                permissions: {}
            },
            notification: [],
            menus: []
        }
    });

    ////////////////////////////////////////////////////////////////////////////
    // Actions
    ////////////////////////////////////////////////////////////////////////////

    loadInitialData = registerAction("grui", "INITIAL_DATA_REQUESTED");

    registerAction("grui", "TITLE_LOADED", (state, {response}) => {
        state.grui.title = response;
        return state;
    });

    changeColorTheme = registerAction("grui", "APP_COLOR_THEME_CHANGED", (state, colorTheme) => {
        colorTheme = colorTheme.toUpperCase()
        if (colorTheme === "DARK" || colorTheme === "LIGHT") {
            state.grui = Object.freeze(Object.assign({}, state.grui, {colorTheme}))
        } else {
            state.grui = Object.freeze(Object.assign({}, state.grui, {colorTheme: "AUTO"}))
        }
        updateColorTheme(state.grui.colorTheme);
        return state;
    });

    registerAction("grui", "SERVICES_LOADED", (state, {response}) => {
        state.grui.service.byId = organizeArrayToMap(response, true, "id");
        return state;
    });

    registerAction("grui", "FUNCTIONS_LOADED", (state, {response}) => {
        state.grui.function.byId = populateDict(state.grui.function.byId, false, "id", response);
        state.grui.function.order = response.sort((a, b) => {
            let tmp = Intl.Collator().compare(a.path, b.path); //a.path.localeCompare(b.path);
            return tmp === 0 ? (HTTP_METHOD_ORDER[a.method] || 5) - (HTTP_METHOD_ORDER[b.method] || 5) : tmp;
        }).map(f => f.id);
        for (let functionId in state.grui.function.byId) {
            state.grui.function.results[functionId] = [];
        }
        return state;
    });

    registerAction("grui", "TYPES_LOADED", (state, {response}) => {
        state.grui.type = populateDict(state.grui.type.byId, true, "id", response)
        return state;
    });

    registerAction("grui", "INITIAL_DATA_LOADED", (state) => {
        let defaultActions = {};
        const allMethodSortedByType = Object.values(state.grui.function.byId).sort((a, b) => {
            return (HTTP_METHOD_ORDER[a.method] || 5) - (HTTP_METHOD_ORDER[b.method] || 5);
        }).map(m => m.id);
        for (let serviceId in state.grui.service.byId) {
            if (serviceId === "grui") {
                continue;
            }
            let serviceMenu = {
                name: serviceId,
                path: "/service/" + serviceId,
                children: []
            }
            const service = state.grui.service.byId[serviceId];
            //console.log("service", service);
            const methods = allMethodSortedByType.filter(id => service.methods.includes(id));
            let returnType = null;
            defaultActions[serviceId] = {
                type: null,
                create: null,
                read: null,
                update: null,
                remove: null
            }
            for (let i = 0; i < methods.length; i++) {
                let method = state.grui.function.byId[methods[i]];
                method.defaultAction = true;
                // console.log("method", method);
                if (method.method === "GET" && method.path === "/api/" + serviceId + "/all" && method.result !== null) {
                    returnType = state.grui.type[method.result.type];
                    //console.log("return Type", returnType);
                    if (returnType.name.startsWith("__array__")) {
                        defaultActions[serviceId].type = returnType.child;
                        defaultActions[serviceId].read = method.id;
                    }
                } else if (method.method === "POST" && method.path === "/api/" + serviceId &&
                        method.result !== null && method.result.type === defaultActions[serviceId].type) {
                    defaultActions[serviceId].create = method.id;
                } else if (method.method === "PUT" && method.path.match("^/api/" + serviceId + "/<.*>$") &&
                        method.result !== null && method.result.type === defaultActions[serviceId].type) {
                    defaultActions[serviceId].update = method.id;
                } else if (method.method === "DELETE" && method.path.match("^/api/" + serviceId + "/<.*>$")) {
                    defaultActions[serviceId].remove = method.id;
                } else {
                    method.defaultAction = false;
                }
                //if (!method.defaultAction) {
                    serviceMenu.children.push({
                        name: method.method + ": " + (method.presentation || method.path),
                        path: "/function/" + method.id
                    });
                //}
            }
            state.grui.menus.push(serviceMenu);
        }

        function sortMenu(a, b) {
            if (a.hasOwnProperty("children")) {
                a.children.sort(sortMenu)
            }
            if (b.hasOwnProperty("children")) {
                b.children.sort(sortMenu)
            }
            return Intl.Collator().compare(a.name, b.name);
        }
        state.grui.menus.push({
            name: "Api Documentation",
            path: "/doc/function/all"
        });
        state.grui.menus.sort(sortMenu);

        state.grui = deepFreeze(Object.assign({}, state.grui, {initialized: true}, {service: {...state.grui.service, defaultActions}}));
        //console.log("INITIALIZED", state.grui);

        return state;
    });

    executeRemoteFunction = registerAction("grui", "EXECUTION_REQUESTED");

    registerAction("grui", "EXECUTION_RETURNED", (state, {functionId, response}) => {
        let results = Object.assign({}, state.grui.function.results);
        results[functionId] = Object.assign([], ...results[functionId]);
        results[functionId].push(response);
        //console.log("results", results)
        state.grui = deepFreeze(Object.assign({}, state.grui, {function:
                Object.assign({}, state.grui.function, {results})}));
        return state;
    });

    actions = getModuleActionTypes("grui")

    ////////////////////////////////////////////////////////////////////////////
    // Saga
    ////////////////////////////////////////////////////////////////////////////

    function* loadInitialGruiData() {
        const initialized = yield select(s => s.grui.initialized);
        if (!initialized) {
            let tasks = [];
            tasks.push(yield fork(generateApiCall("/api/grui/title", "GRUI", actions.TITLE_LOADED), {}));
            tasks.push(yield fork(generateApiCall("/api/grui/type/all", "GRUI", actions.TYPES_LOADED), {}));
            tasks.push(yield fork(generateApiCall("/api/grui/service/all", "GRUI", actions.SERVICES_LOADED), {}));
            tasks.push(yield fork(generateApiCall("/api/grui/function/all", "GRUI", actions.FUNCTIONS_LOADED), {}));
            yield join(tasks)
            yield put({type: actions.INITIAL_DATA_LOADED});
        }
    }

    function* executeFunctionFromApi({payload}) {
        let {functionId, args} = payload;
        let {path, method} = yield select(s => s.grui.function.byId[functionId]);
        args = Object.assign({}, args);
        for (const argName in args) {
            let tmp = path.replace("<" + argName + ">", args[argName]);
            if (tmp !== path) {
                path = tmp;
                delete args[argName];
            }
        }
        let body = args;
        if (Object.keys(args).length === 1) {
            body = args[Object.keys(args)[0]];
        }
        console.log("executeFunctionFromApi", payload, path, body);
        try {
            const response = yield call(getApiCallGyMethod(method), path, body);
            yield put({type: actions.EXECUTION_RETURNED, payload: {functionId, response}});
        } catch (e) {
            yield put({type: apiActions.ERROR, payload: {url : path, method, payload, body, exception: e}});
        }
    }

    registerSagaWatcher(function* watcherSaga() {
        yield takeEvery(actions.INITIAL_DATA_REQUESTED, loadInitialGruiData);
        yield takeEvery(actions.EXECUTION_REQUESTED, executeFunctionFromApi);
        // yield takeEvery(actions.INITIALIZED, loadInitialGruiData);
    })

}

export { actions as default, loadInitialData, changeColorTheme, executeRemoteFunction }
