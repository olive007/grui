import React, { useEffect, useRef, useState } from 'react';
import { useSelector } from 'react-redux';
import { noop } from "../../../util";

const TYPE_BY_NAME = {
    "__int__": "number",
    "__float__": "number",
    "__bool__": "checkbox",
    "__str__": "text"
};

const DEFAULT_VALUE_BY_NAME = {
    "__int__": 0,
    "__float__": 0.,
    "__bool__": false,
    "__str__": ""
};

const GET_VALUE_FUNC = {
    "__int__": (input) => parseInt(input.value),
    "__float__": (input) => parseFloat(input.value),
    "__bool__": (input) => input.checked,
    "__str__": (input) => input.value
};

/**
 * Also update the height when the user press 'Enter'
 */
function Textarea({label, path, defaultValue, onChange = noop, ...props}) {

    const ref = useRef(null);
    defaultValue = defaultValue === undefined || defaultValue === null ? "" : defaultValue;
    const [value, setValue] = useState(defaultValue)

    // A simple function to update the height of textarea to the height of the content.
    function updateHeight(textareaNode) {
        if (textareaNode !== undefined && textareaNode !== null) {
            let outerHeight = parseInt(window.getComputedStyle(textareaNode).height, 10);
            let diff = outerHeight - textareaNode.clientHeight;
            textareaNode.style.height = 0;
            textareaNode.style.height = Math.max(100, textareaNode.scrollHeight + diff) + 'px';
        }
    }
    function handleChange(event) {
        updateHeight(event.target)
        //console.log("handleChange TEXT", event, path, event.target.value);
        setValue(event.target.value);
        onChange(event, path, event.target.value);
    }

    useEffect(() => {
        updateHeight(ref.current);
    }, [ref]);

    return <textarea onChange={handleChange} value={value} ref={ref} {...props}/>
}

export default function Input({type, path, defaultValue = null, onChange = noop}) {

    const gruiType = useSelector(s => s.grui.type[type]);
    defaultValue = defaultValue === null ? DEFAULT_VALUE_BY_NAME[gruiType.name] : defaultValue;
    const [value, setValue] = useState(defaultValue);

    function handleChange(event) {
        const getter = GET_VALUE_FUNC[gruiType.name] || GET_VALUE_FUNC["__str__"];
        //console.log("handleChange INP", event, path, getter(event.target));
        setValue(getter(event.target));
        onChange(event, path, getter(event.target));
    }

    if (gruiType.name === "__text__") {
        return <Textarea {...arguments[0]}/>
    } else {
        return <input type={TYPE_BY_NAME[gruiType.name]}
                      value={value}
                      onChange={handleChange}
                      step={gruiType.name === "__float__" ? "any" : null}/>
    }
}