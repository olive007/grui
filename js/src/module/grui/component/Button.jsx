import React from "react";

import '../style/button.scss'

export default function({label, Icon, color = "", ...other}) {
    return <button className={color + " pure-button"} {...other}>{label} {Icon === undefined ? null : <span><Icon/></span>}</button>
}