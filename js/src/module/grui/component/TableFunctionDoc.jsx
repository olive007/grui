import React from 'react'
import { useSelector } from "react-redux";

import Table from "./Table";

import '../style/table-function-doc.scss'

function Method({value}) {
    return <span className={"method " + value.toLowerCase()}>{value}</span>
}

export default function() {

    const data = useSelector(s => s.grui.function.order.map(id => s.grui.function.byId[id]));
    const columns = [{
        Header: "Method",
        accessor: v => <Method value={v.method}/>
    }, {
        Header: "Path",
        accessor: "path"
    }, {
        Header: "Arguments",
        accessor: func => Object.keys(func.args).join(",")
    }, {
        Header: "Presentation",
        accessor: "presentation"
    }];

    return <Table className={"function-doc"} data={data} columns={columns} />
}