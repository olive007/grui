import React, { useEffect } from 'react';
import { Provider, useDispatch, useSelector } from "react-redux";
import { BrowserRouter  as Router } from 'react-router-dom'

import store from '../../../../redux/store'
import Menu from "./Menu";
import Header from './Header'
import Body from './Body'
import Footer from "./Footer";

import { updateColorTheme } from '../../color'
import { loadInitialData } from "../../redux";

import 'normalize.css/normalize.css';
import 'purecss/build/pure-min.css'
import 'purecss/build/grids-responsive-min.css'
import '../../style/grui-app.scss'

function App() {

    const dispatch = useDispatch();
    const initialDataComputed = useSelector(s => s.grui.initialized)

    useEffect(() => {
        dispatch(loadInitialData());
    }, [dispatch]);

    if (initialDataComputed) {
        return <>
            <Menu />
            <div className="right-part">
                <Header />
                <div className="content">
                    <Body />
                    <Footer />
                </div>
            </div>
        </>
    } else {
        return <div>Application loading...</div>;
    }
}

export default function() {

    useEffect(() => {
        updateColorTheme();
        window.matchMedia('(prefers-color-scheme: dark)').addEventListener("change", updateColorTheme)
        return () => {
            window.matchMedia('(prefers-color-scheme: dark)').removeEventListener("change", updateColorTheme)
        }
    });

    return <Provider store={store}>
        <Router>
            <App/>
        </Router>
    </Provider>;
}