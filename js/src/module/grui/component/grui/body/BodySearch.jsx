import React from 'react';

export default function({query}) {

    return <>
        <h2>Search result: {query}</h2>
        <ul>
        </ul>
    </>
}