import React from 'react';

import { useSelector } from "react-redux";

export default function() {

    const presentation = useSelector(s => s.grui.presentation)
    const user = useSelector(s => s.grui.users.authenticated)

    return <div className="body-home">
        <h2>Welcome on this application</h2>
        <p>{user === null ? "You aren't authenticated" : "You are authenticated as " + user.name}</p>
        <p>{presentation}</p>
    </div>
}