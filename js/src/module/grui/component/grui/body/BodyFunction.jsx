import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { executeRemoteFunction } from "../../../redux";

import BodyNotFound from './BodyNotFound'
import Form from '../../Form'

export default function BodyFunction({functionId, values = {}}) {

    const dispatch = useDispatch();
    const function_ = useSelector(s => s.grui.function.byId[functionId]);
    const lastResult = useSelector(s => s.grui.function.results[functionId].slice(-1)[0]);
    if (function_ === undefined) {
        return <BodyNotFound/>
    }
    const {presentation, description, args} = function_;

    function executeFunction(event, args) {
        console.log("executeFunction", args);
        dispatch(executeRemoteFunction({functionId, args}));
    }

    return <div className="body-function">
        <h2>{presentation}</h2>
        <p>{description}</p>
        <hr/>
        <div className="pure-g">
            <div className="pure-u-1 pure-u-md-1-2">
                <h3>Parameters</h3>
                <Form args={args} onSubmit={executeFunction} />
            </div>
            <div className="pure-u-1 pure-u-md-1-2">
                <h3>Result</h3>
                <p>{JSON.stringify(lastResult)}</p>
            </div>
        </div>
    </div>
}