import React, { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { FaPlus, FaEdit, FaTrashAlt } from "react-icons/fa/index";
import { Route, Switch, useHistory } from "react-router-dom";

import { EMPTY_ARRAY } from "../../../../../util";
import { loadDefaultServiceData } from "../../../../data/redux";

import Table from "../../Table";
import Button from "../../Button";
import BodyNotFound from './BodyNotFound'
import BodyFunction from './BodyFunction'

function Actions({dataType, match, table, update, remove}) {

    const history = useHistory();

    function pushAction(type, value) {
        if (dataType.model) {
            history.push(match.url + "/" + type + "/" + value.id);
        } else {
            history.push(match.url + "/" + type + "/" + table.row.index);
        }
    }

    return <div>
        {update ? <Button color="blue" label="Edit" Icon={FaEdit} onClick={e => pushAction("edit", table.row.original)}/> : null}
        {remove ? <Button color="red" label="Delete" Icon={FaTrashAlt} onClick={e => pushAction("delete", table.row.original)}/> : null}
    </div>
}

const columnsByType = {};

function TableDefaultAction({serviceId, type, update, remove, match}) {

    const dataType = useSelector(s => s.grui.type[type]);
    const data = useSelector(s => {
        if (dataType.model) {
            let tmp = s.data[type];
            return tmp !== undefined ? tmp.order.map(id => tmp.byId[id]) : EMPTY_ARRAY;
        } else {
            return s.data[serviceId + dataType.name];
        }
    });
    if (columnsByType[type] === undefined) {
        columnsByType[type] = [];
        if (dataType.model) {
            Object.keys(dataType.children).forEach(id => {
                columnsByType[type].push({
                    Header: id,
                    accessor: value => value[id] === null ? "" : value[id].toString()
                })
            });
        } else {
            columnsByType[type].push({
                Header: "Value",
                accessor: value => value
            });
        }
    }
    let columns = [...columnsByType[type]];
    let className = "";
    if (update || remove) {
        className = "with-actions";
        columns.push({
            Header: " ",
            Cell: table => <Actions dataType={dataType} match={match} table={table} update={update} remove={remove}/>
        });
    }

    return <>
        <Table className={className} data={data} columns={columns}/>
    </>
}

export default function({match}) {

    const serviceId = match.params.id;
    const dispatch = useDispatch();
    const history = useHistory();
    const service = useSelector(s => s.grui.service.byId[serviceId]);
    const actions = useSelector(s => s.grui.service.defaultActions[serviceId]);

    useEffect(() => {
        dispatch(loadDefaultServiceData(serviceId))
    });

    function addAction() {
        history.push(match.url + "/add");
    }

    return <Switch>
        <Route path={match.url + "/add"} render={({match}) => <BodyFunction functionId={actions.create} values={match.params}/>}/>
        <Route path={match.url + "/edit/:id"} render={({match}) => <BodyFunction functionId={actions.update} values={match.params}/>}/>
        <Route path={match.url + "/delete/:id"} render={({match}) => <BodyFunction functionId={actions.remove} values={match.params}/>}/>
        <Route exact path={match.url}>
            <h2>Service: {service.name}</h2>
            <p>{service.description}</p>
            {actions.create ? <p><Button color="green" label="Insert" Icon={FaPlus} onClick={addAction}/></p> : null}
            {actions.type !== null && actions.read ? <TableDefaultAction serviceId={serviceId} {...actions} match={match}/> : null}
        </Route>
        <Route>
            <BodyNotFound/>
        </Route>
    </Switch>
}