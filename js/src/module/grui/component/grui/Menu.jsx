import React, { useEffect, useRef } from "react";
import { Link, useHistory, useRouteMatch } from "react-router-dom";
import { useSelector } from "react-redux";
import { EMPTY_ARRAY } from "../../../../util";

function MenuLink({name, path, children = EMPTY_ARRAY}) {

    const match = useRouteMatch(path);

    return <li className={"pure-menu-item" + (match !== null ? " pure-menu-selected" : "")}>
        <Link className="pure-menu-link" to={path} replace>
            {name}
        </Link>
        {children.length === 0 ? null :
            <ul className="pure-menu-list">
                {children.map(child => <MenuLink key={child.path} {...child}/>)}
            </ul>
        }
    </li>
}

function GoBack() {
    let history = useHistory();

    function handleClick() {
        history.goBack()
    }

    return (
        <button type="button" onClick={handleClick}>
            Go back
        </button>
    );
}

export default function() {

    const ref = useRef(null);
    const appName = useSelector(s => s.grui.title);
    const menus = useSelector(s => s.grui.menus);

    useEffect(() => {
        function resizeListener() {
            const ratio = ref.current.scrollWidth / ref.current.clientWidth; // Get the ratio between the displayable text and real size
            const fontSize = parseFloat(window.getComputedStyle(ref.current, null).getPropertyValue('font-size'))
            if (ratio === 1) {
                // If the ratio is 1 the text could be too small ...
                ref.current.style.fontSize = "1600px";
                // ... so we restart with a big font size
                resizeListener();
            } else {
                // removing 80  allow the title to have a margin on the right and left
                let newSize = (fontSize - 80) / ratio;
                newSize = newSize >= 36. ? 36. : newSize;
                ref.current.style.fontSize = newSize+"px";
            }
        }
        resizeListener()
        window.addEventListener('resize', resizeListener);

        return () => {
            window.removeEventListener('resize', resizeListener);
        }
    }, [ref])

    return <div className="menu">
        <div className="pure-menu">
            <Link className="pure-menu-heading" to={"/"} innerRef={ref}>
                {appName}
            </Link>
            <ul className="pure-menu-list">
                {menus.map(m => <MenuLink key={m.path} {...m}/>)}
                <li><GoBack/></li>
            </ul>
        </div>
    </div>
}