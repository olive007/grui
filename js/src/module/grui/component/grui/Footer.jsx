import React from "react";
import { useSelector } from "react-redux";

function ApiError({url = "", method = "", body = "", exception}) {

    return <li>
        Api error: <b>{method}</b> {url} {JSON.stringify(body)}
    </li>
}

export default function() {

    const apiErrors = useSelector(s => s.api.errors)

    return <div className="footer">
        <h2>Footer</h2>
        <ul>
            {apiErrors.map(e => <ApiError key={e.id} {...e}/>)}
        </ul>
    </div>
}