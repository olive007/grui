import React from 'react';
import { Switch, Route } from "react-router-dom";
import { useLocation } from "react-router-dom"

import BodyFunction from './body/BodyFunction'
import BodySearch from './body/BodySearch'
import BodyService from './body/BodyService'
import BodyHome from './body/BodyHome'
import BodyNotFound from './body/BodyNotFound'
import TableFunctionDoc from "../TableFunctionDoc";

export default function() {

    const currentPath = useLocation().pathname.split("/").filter(s => s.length !== 0);

    //console.log("currentPath", currentPath, useLocation())

    return <>
        <div className="navigation-bar">
            {"home" + currentPath.map(p => " > " +p).join("")}
        </div>
        <hr/>
        <div className="body">
            <Switch>
                <Route path="/function/:id" render={({match}) => <BodyFunction functionId={match.params.id}/>}/>
                <Route path="/doc/function/all" component={TableFunctionDoc}/>
                <Route path="/search/:query" render={({match}) => <BodySearch query={match.params.query}/>}/>
                {/*<Route path="/service/:id" render={({match}) => <BodyService serviceId={match.params.id}/>}/>*/}
                <Route path="/service/:id" component={BodyService}/>
                <Route exact path="/" component={BodyHome}/>
                <Route component={BodyNotFound}/>
            </Switch>
        </div>
    </>
}