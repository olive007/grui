import React, { useEffect, useRef } from "react";
import { FaMoon, FaSun, FaSearch, FaRegBookmark, FaBookmark, FaUserAlt, FaUserSlash } from 'react-icons/fa';
import { GiHamburgerMenu } from 'react-icons/gi'
import { MdBrightnessAuto } from 'react-icons/md'
import { useHistory, useRouteMatch } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { changeColorTheme } from "../../redux";

function Menu() {
    return <div className="menu">
        <span><GiHamburgerMenu/></span>
    </div>
}

const THEMES = ["LIGHT", "DARK", "AUTO"]

function ColorThemeSelector() {

    const dispatch = useDispatch();
    const theme = useSelector(state => state.grui.colorTheme);

    function setTheme(theme) {
        let width = (window.innerWidth > 0) ? window.innerWidth : window.screen.width;
        if (width < 900) {
            let index = THEMES.indexOf(theme) + 1;
            index = index >= 3 ? 0 : index;
            dispatch(changeColorTheme(THEMES[index]))
        } else {
            dispatch(changeColorTheme(theme))
        }
    }

    return <div className="color-theme-selector">
        <span onClick={() => setTheme("LIGHT")} className={theme === "LIGHT" ? "selected" : ""}><FaSun/></span>
        <span onClick={() => setTheme("DARK")} className={theme === "DARK" ? "selected" : ""}><FaMoon/></span>
        <span onClick={() => setTheme("AUTO")} className={theme === "AUTO" ? "selected" : ""}><MdBrightnessAuto/></span>
    </div>;
}

function Search() {
    const history = useHistory();
    const match = useRouteMatch("/search/:query");
    const ref = useRef(null)

    function searchAction(event) {
        event.preventDefault();
        history.push("/search/" + event.target[0].value);
        return false;
    }

    useEffect(() => {
        ref.current.value = match === null ? "" : match.params?.query || "";
    })

    return <div className="search">
        <form onSubmit={searchAction}>
            <input name="search-term" placeholder="Search action" type="text" ref={ref}/>
            <button type="submit"><span><FaSearch/></span></button>
        </form>
    </div>
}

function Notification() {
    const nbrNotification = useSelector(state => state.grui.notification.length);

    return <div className="notification">
        {nbrNotification <= 0 ? <span><FaRegBookmark/></span> :
            <span><FaBookmark/><span className="badge">{nbrNotification <= 100 ? nbrNotification : "99+"}</span></span>}
    </div>
}

function UserAuthenticated() {
    const user = useSelector(state => state.grui.users.authenticated);

    return <div className="user-authenticated">
        <span>{user === null ? <FaUserSlash/> : <><FaUserAlt/><span>{user.name}</span></>}</span>
    </div>
}

export default function() {

    return <div className="header">
        <Menu />
        <ColorThemeSelector />
        <Search />
        <Notification/>
        <UserAuthenticated />
    </div>
}