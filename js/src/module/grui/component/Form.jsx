import React, { useState } from 'react';
import Input from './Input';
import Button from './Button';
import { noop } from "../../../util";
import { useSelector } from "react-redux";

import '../style/form.scss'

function InputGroup({type, path, description, defaultValue, onChange = noop}) {

    const gruiType = useSelector(s => s.grui.type[type]);

    return <>
        <h4>{description}</h4>
        {gruiType.model ? <>
            {Object.keys(gruiType.children).map(argName => {
                const {readOnly, description} = gruiType.children[argName];
                return readOnly ? null : <div key={argName} className="pure-g">
                    <div className="pure-u-1-5">
                        <label>{argName}</label>
                    </div>
                    <div className="pure-u-4-5">
                        <Input path={[...path, argName]} {...gruiType.children[argName]} onChange={onChange}/>
                        <p className="help-message">{description}</p>
                    </div>
                </div>;
            })}
        </> : <Input path={path} {...arguments[0]}/>/* reset the description to none as it is repeated otherwise */}
    </>
}

export default function Form({args, defaultValue = {}, onSubmit = noop}) {

    const [value, setValue] = useState(defaultValue);

    function handleChange(event, path, newValue) {
        let tmp = value;
        let i;
        for (i = 0; i + 1 < path.length; i++) {
            const key = path[i];
            if (tmp[key] === undefined) {
                tmp[key] = {}
            }
            tmp = tmp[key];
            //console.log("FormModel path", key, tmp, tmp[key]);
        }
        const key = path[i];
        tmp[key] = newValue
        setValue(value);
        //console.log("FormModel handleChange", value);
    }

    function submitAction(event) {
        event.preventDefault();
        console.log("submitAction", event, value);
        return onSubmit(event, value);
    }

    return <form className="pure-form" onSubmit={submitAction}>
        {Object.keys(args).length === 0 ? <p>No Parameter for this function</p> :
            Object.keys(args).map(name => <div key={name}>
                <InputGroup path={[name]} {...args[name]} onChange={handleChange}/>
                <hr/>
            </div>)
        }
        <Button type="submit" color={"blue"} label="Execute"/>
    </form>;
}