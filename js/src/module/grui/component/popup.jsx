import React, { useState } from "react";
import Popup from "reactjs-popup";

import 'reactjs-popup/dist/index.css';

function Toto({value = null}) {
    return <div>TOTO {value}</div>
}
export default function Pop() {
    const [open, setOpen] = useState(false);
    const closeModal = () => setOpen(false);

    const Test = Toto
    return (
        <div>
            <button type="button" className="button" onClick={() => setOpen(o => !o)}>
                Controlled Popup
            </button>
            <Popup open={open} closeOnDocumentClick onClose={closeModal}>
                <div className="modal">
                    <Test value={"Salut"}/>
                    <a className="close" onClick={closeModal}>
                        &times;
                    </a>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae magni
                    omnis delectus nemo, maxime molestiae dolorem numquam mollitia, voluptate
                    ea, accusamus excepturi deleniti ratione sapiente! Laudantium, aperiam
                    doloribus. Odit, aut.
                </div>
            </Popup>
        </div>
    );
    return <Popup trigger={<button className="button"> Open Modal </button>} modal>
            <span> Modal content </span>
        </Popup>
    return <Popup trigger={<button> Trigger</button>} position="right center">
        <div>Popup content here !!</div>
    </Popup>
}