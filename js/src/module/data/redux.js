import { takeEvery, select, call } from "@redux-saga/core/effects";
import { registerInitialState, registerAction, registerSagaWatcher, getModuleActionTypes } from "../../redux/action";
import { generateApiGetCall } from "../../redux/api";
import { deepFreeze } from "../../util";

let dataActions;
let loadData;
let loadDefaultServiceData;

export function register() {

    ////////////////////////////////////////////////////////////////////////////
    // Initial State
    ////////////////////////////////////////////////////////////////////////////

    registerInitialState({
        data: {}
    });

    ////////////////////////////////////////////////////////////////////////////
    // Actions
    ////////////////////////////////////////////////////////////////////////////

    loadData = registerAction("data", "COLLECTION_REQUESTED");
    loadDefaultServiceData = registerAction("data", "DEFAULT_SERVICE_REQUESTED");

    registerAction("data", "LOADED", (state, {response, type, collection, callWith: serviceId}) => {
        console.log("Response", response);
        if (type.model) {
            let byId = {};
            let order = [];

            for (let i = response.length; --i >= 0;) {
                // By freezing the object we avoid any unwanted modification !
                byId[response[i].id] = deepFreeze(response[i]);
                order.unshift(response[i].id);
            }
            let oldById = {};
            let oldOrder = Object.freeze([]);

            if (state.data[type.id] !== undefined) {
                oldById = state.data[type.id].byId;
                oldOrder = state.data[type.id].order;
            }
            state.data[type.id] = Object.freeze({
                byId: Object.freeze(Object.assign({}, oldById, byId)),
                order: collection ? Object.freeze(order) : oldOrder
            });
        } else {
            state.data[serviceId + type.name] = deepFreeze(response);
        }
        console.log("Response 2 ", response, state.data);
        return state;
    });

    dataActions = getModuleActionTypes("data");

    ////////////////////////////////////////////////////////////////////////////
    // Saga
    ////////////////////////////////////////////////////////////////////////////

    function* researchDefaultServiceData({url}) {
        yield call(generateApiGetCall(url, dataActions.LOADED), {url});
    }

    function* loadDefaultServiceDataFromApi({payload: serviceId}) {
        const url = "/api/" + serviceId + "/all";
        const type = yield select(((s, id) => s.grui.type[s.grui.service.defaultActions[id].type]), serviceId);
        yield call(generateApiGetCall(url, dataActions.LOADED, {type, collection: true}), {payload: serviceId});
    }

    registerSagaWatcher(function* watcherSaga() {
        yield takeEvery(dataActions.COLLECTION_REQUESTED, researchDefaultServiceData);
        //yield takeEvery(reduxActionTypes.DEFAULT_SERVICE_REQUESTED, generateApiGetCall(
        //    (s, p) => "/" + p + "/all", reduxActionTypes.LOADED, {type: null, collection: true}));
        yield takeEvery(dataActions.DEFAULT_SERVICE_REQUESTED, loadDefaultServiceDataFromApi);
    })
}

export { dataActions as default, loadData, loadDefaultServiceData }
